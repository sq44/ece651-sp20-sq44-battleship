package edu.duke.sq44.battleship;

import java.io.Serializable;

/**
 * Class used for getting display information
 * Currently only have two, one is for being hit another is for normal data;
 * Only work for character now, but can be extented in the future since I use T
 * @param <T>
 */
public class SimpleShipDisplayInfo<T>  implements ShipDisplayInfo<T>, Serializable {
    private T myData;
    private T onHit;
    public SimpleShipDisplayInfo(T data, T hit){
        this.myData = data;
        this.onHit = hit;
    }

    public T getInfo(Coordinate where, boolean hit){
        return hit ? onHit : myData;
    }
}

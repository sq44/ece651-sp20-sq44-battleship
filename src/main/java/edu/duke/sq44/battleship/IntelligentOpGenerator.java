package edu.duke.sq44.battleship;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * generate intelligent operation according to given info
 * it can do the three different choice and record the helpful info, Eg .sonar scan info will be used for next fire, and fire
 * are not likely to the miss/hit part in the past(still has lower possibility to fire randomly, considering the player can move new
 * ship into the old fired place). And if some fires are successful, firing the nearing neighbor point around
 * the successful fired points has higher priority when choosing the fire operation.
 */
public class IntelligentOpGenerator {
    Set<Coordinate> hasFired;
    Set<Coordinate> toBeFired;
    int operationThreshHold;
    int validFireCount;

    public IntelligentOpGenerator() {
        this.hasFired = new HashSet<>();
        this.toBeFired = new HashSet<>();
        this.operationThreshHold = 20;
        this.validFireCount = 0;
    }

    //choose in three choice with same possibility
    public String generateChoice() {
        String[] choiceTable = new String[]{"F", "M", "S"};
        int choice = (int) (Math.random() * 3);
        return choiceTable[choice];
    }

    /**
     * computer will firstly find all fired point, then randomly fire the nearest eight point nearest to
     * one of the fired point, and first pattern will look like spread out
     * the fired points will be recorded in history and will not be fired again
     * If the computer choose fire more than threshold(get by test) but still get invalid target, it means it may have already fire the
     * given area, then it will to fire the point other than this area.
     * @param enemyBoard
     * @return operation for computer to fire at
     */
    public Coordinate generateFire(Board<Character> enemyBoard) {
        validFireCount ++;
        if (validFireCount > 200) hasFired.clear(); //it means player has moved some ship to the fired place, computer need to clear and find out
        //System.out.println(validFireCount);
        //toBeFired has the highest priority
        for (Coordinate c: toBeFired) {
            if (hasFired.contains(c)) {
                continue;
            }
            hasFired.add(c);
            return c;
        }
        int h = enemyBoard.getHeight();
        int w = enemyBoard.getWidth();
        List<Coordinate> firedTargets = findPotentialTargets(enemyBoard, h, w);
        int sz = firedTargets.size();
        if (sz == 0) {
            return getRandCoordinate(h, w);
        }
        //set threshold for random times
        for (int count = 0; count < operationThreshHold; count ++) {
            Coordinate centerTarget = firedTargets.get((int)(Math.random() * sz));
            int row = centerTarget.getRow();
            int col = centerTarget.getColumn();
            int[][] dir = new int[][] {{1, 1}, {1, -1}, {-1, 1}, {-1, -1}, {1, 0}, {-1, 0}, {0, 1}, {0, -1}};
            for (int i = 0; i < 8; i ++) {
                if (row + dir[i][0] >= h || row + dir[i][0] < 0 || col + dir[i][1] < 0 || col + dir[i][1] >= w
                        || hasFired.contains(new Coordinate(row + dir[i][0], col + dir[i][1]))) continue;
                Coordinate fireTarget = new Coordinate(row + dir[i][0], col + dir[i][1]);
                hasFired.add(fireTarget);
                return fireTarget;
            }
        }
        Coordinate fireTarget = findRemainingTarget(enemyBoard, h, w);
        hasFired.add(fireTarget);
        return fireTarget;
    }
    //find all fired target and store into set;need update every time since player can move
    private List<Coordinate> findPotentialTargets(Board<Character> board, int h, int w) {
        List<Coordinate> firedTargets = new ArrayList<>();
        for (int r = 0; r < h; r ++) {
            for (int c = 0; c < w; c ++) {
                Coordinate curr = new Coordinate(r,c);
                //I use owner board display, this can avoid the situation if player move to a fired point again, computer will never find out
                if (board.whatIsAtForSelf(curr) != null && board.whatIsAtForSelf(curr) == '*') { //if found fired successfully
                    firedTargets.add(curr);
                }
            }
        }
        return firedTargets;
    }

    private Coordinate findRemainingTarget(Board<Character> board, int h, int w) {
        List<Coordinate> firedTargets = new ArrayList<>();
        for (int r = 0; r < h; r ++) {
            for (int c = 0; c < w; c ++) {
                Coordinate curr = new Coordinate(r,c);
                //I use owner board display, this can avoid the situation if player move to a fired point again, computer will never find out
                if (board.whatIsAtForEnemy(curr) == null) { //if found fired successfully
                    firedTargets.add(curr);
                }
            }
        }
        return firedTargets.get((int)(Math.random() * firedTargets.size()));
    }

    private Coordinate getRandCoordinate(int h, int w) {
        Coordinate randTarget = new Coordinate((int)(Math.random() * h), (int)(Math.random() * w));
        //get target until no repetition
        while (hasFired.contains(randTarget)) randTarget = new Coordinate((int)(Math.random() * h), (int)(Math.random() * w));
        hasFired.add(randTarget);
        return randTarget;
    }
    //no need to become intelligent since random move is already very comfusing for player
    public Coordinate generateMoveTarget(Board<Character> theBoard) {
        int r = theBoard.getHeight();
        int c = theBoard.getWidth();
        return new Coordinate((int) (Math.random() * r), (int) (Math.random() * c));
    }
    public Placement generateMovePlacement(Board<Character> theBoard) {
        int r = theBoard.getHeight();
        int c = theBoard.getWidth();
        char[] orientationTable = new char[]{'V', 'H', 'U', 'D', 'L', 'R'};
        Placement moveTo = new Placement(new Coordinate((int) (Math.random() * r), (int) (Math.random() * c)), orientationTable[(int) (Math.random() * 6)]);
        return moveTo;
    }

    public Coordinate generateSonar(Board<Character> enemyBoard) {
        int r = enemyBoard.getHeight();
        int c = enemyBoard.getWidth();
        List<Coordinate> firedTargets = findPotentialTargets(enemyBoard, r, c);
        int sz = firedTargets.size();
        if (sz == 0) {
            return getRandCoordinate(r, c);
        }
        Coordinate centerTarget = firedTargets.get((int)(Math.random() * sz));
        return centerTarget;
    }
}

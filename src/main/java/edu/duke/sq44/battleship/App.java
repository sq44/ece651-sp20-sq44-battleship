package edu.duke.sq44.battleship;

import java.io.*;
import java.util.Scanner;

/**
 * local version, adding new features: intelligent computer AI
 * this class applies input and output stream, manage result display
 * containing read, place and fire operation
 * check win or lose condition
 */
public class App {
    final TextMaster player1;
    final TextMaster player2;
    public App(TextMaster player1, TextMaster player2) {
        this.player1 = player1;
        this.player2 =  player2;
    }


    public static void main(String[] args) throws IOException {
        boolean checkOptionValid = false;
        System.out.print("Hi! which kind of game would you like to choose?\n" +
                "1: PVP\n" +
                "2: PVE\n" +
                "3: EVP\n" +
                "4: EVE\n");
        Scanner s = new Scanner(System.in);
        while (!checkOptionValid) {
            switch (s.nextLine()) {
                case "1":
                    pvpMode();
                    checkOptionValid = true;
                    break;
                case "2":
                    pveMode();
                    checkOptionValid = true;
                    break;
                case "3":
                    evpMode();
                    checkOptionValid = true;
                    break;
                case "4":
                    eveMode();
                    checkOptionValid = true;
                    break;
                default:
                    System.out.println("invalid option, please input 1, 2, 3, or 4");
            }
        }
    }

    public static void pvpMode() throws IOException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
        V2ShipFactory factory = new V2ShipFactory();

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        TextPlayer p1 = new TextPlayer("A", b1, input, System.out, factory);
        TextPlayer p2 = new TextPlayer("B", b2, input, System.out, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }
    //pve and new intelligent pc
    public static void pveMode() throws IOException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
        V2ShipFactory factory = new V2ShipFactory();

        BufferedReader input1 = new BufferedReader(new InputStreamReader(System.in));
        TextPlayer p1 = new TextPlayer("A", b1, input1, System.out, factory);

        InputStream cpStream = App.class.getClassLoader().getResourceAsStream("input.txt");
        BufferedReader input2 = new BufferedReader(new InputStreamReader(cpStream));
        //TextPlayer p2 = new TextPlayer("B", b2, input2, System.out, factory);
        TextComputer p2 = new TextComputer("B", b2, input2, System.out, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }
    //evp and using old version of cp by comparison
    public static void evpMode() throws IOException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
        V2ShipFactory factory = new V2ShipFactory();

        BufferedReader input2 = new BufferedReader(new InputStreamReader(System.in));
        TextPlayer p2 = new TextPlayer("A", b2, input2, System.out, factory);

        InputStream cpStream = App.class.getClassLoader().getResourceAsStream("inputWithSM.txt");
        BufferedReader input1 = new BufferedReader(new InputStreamReader(cpStream));
        TextPlayer p1 = new TextPlayer("B", b1, input1, System.out, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }
    //old pc vs intelligent pc
    public static void eveMode() throws IOException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
        V2ShipFactory factory = new V2ShipFactory();

        InputStream cpStream1 = App.class.getClassLoader().getResourceAsStream("input.txt");
        BufferedReader input1 = new BufferedReader(new InputStreamReader(cpStream1));
        TextPlayer p1 = new TextPlayer("A", b1, input1, System.out, factory);

        InputStream cpStream2 = App.class.getClassLoader().getResourceAsStream("input.txt");
        BufferedReader input2 = new BufferedReader(new InputStreamReader(cpStream2));
        //TextPlayer p2 = new TextPlayer("B", b2, input2, System.out, factory);
        TextComputer p2 = new TextComputer("B", b2, input2, System.out, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }

    /**
     * Placement phase for both players, one for 10 turns
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }

    /**
     * attacking phase for both players,one for one turn until one player win
     * @throws IOException
     */
    public void doAttackingPhase() throws IOException {
        while (true) {
            player1.playOneTurn(player2.getBoard(), "Player B");
            if (player1.checkWin(player2.getBoard())) return;

            player2.playOneTurn(player1.getBoard(), "Player A");
            if(player2.checkWin(player1.getBoard())) return;
        }
    }
}

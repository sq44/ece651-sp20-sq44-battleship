package edu.duke.sq44.battleship;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * this class is the board that contain the information
 * of the battleship board, which can be used  to
 * play and display in the future
 */
public class BattleShipBoard<T> implements Board<T>, Serializable {
    final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    private final int width;
    private final int height;
    private HashSet<Coordinate> enemyMisses;
    private HashMap<Coordinate, T> enemyStuckHit; // for the coordinate being moved but stuck display in enemy board
    private final T missInfo;

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return  height;
    }
    /**
     * Constructs a BattleShipBoard with the specified width
     * and height
     * @param w is the width of the newly constructed board.
     * @param h is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard(int w, int h, T missInfo, PlacementRuleChecker<T> placementChecker) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.width = w;
        this.height = h;
        this.missInfo = missInfo;
        this.myShips = new ArrayList<>();
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<>();
        this.enemyStuckHit = new HashMap<>();
    }

    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, missInfo, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<>(null)));
    }

    public String tryAddShip(Ship<T> toAdd) {
        if (placementChecker.checkPlacement(toAdd, this) == null) {
            myShips.add(toAdd);
            return null;
        }
        return placementChecker.checkPlacement(toAdd, this);
    }

    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if (!isSelf) { //enemy view
            if (enemyMisses.contains(where)) {
                return missInfo;
            }
            if (enemyStuckHit.containsKey(where)) {
                return enemyStuckHit.get(where);
            }
        } else { //myself view
            for (Ship<T> s: myShips) {
                if (s.occupiesCoordinates(where)){
                    return s.getDisplayInfoAt(where, true);
                }
            }
        }
        return null;
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    public Ship<T> fireAt(Coordinate c){
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(c)){
                s.recordHitAt(c);
                if (enemyStuckHit.containsKey(c)) { // has stuck then remove and update
                    enemyStuckHit.remove(c);
                }
                enemyStuckHit.put(c, s.getDisplayInfoAt(c, false)); //get display for enemy and stuck it
                enemyMisses.remove(c); //possible when first miss then hit
                return s;
            }
        }

        enemyMisses.add(c);
        return null;
    }

    public boolean allSunk() {
        for (Ship<T> s: myShips) {
            if (!s.isSunk()) return false;
        }
        return true;
    }

    public ArrayList<Ship<T>> getShips() {
        return myShips;
    }

    @Override
    public String toString() {
        return new BoardTextView(this).displayMyOwnBoard(); //todo: change to for enemy
    }
}

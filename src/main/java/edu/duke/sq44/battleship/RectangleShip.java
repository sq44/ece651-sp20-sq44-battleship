package edu.duke.sq44.battleship;

import java.io.Serializable;
import java.util.HashSet;

/**
 * class for shape as rectangle, including info: name, place, hit
 * @param <T> presenting element
 */
public class RectangleShip<T> extends BasicShip<T> implements Serializable {
    private final String name;
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> displayInfoA, ShipDisplayInfo<T> displayInfoB) {
        super(makeCoords(upperLeft, width, height), displayInfoA, displayInfoB);
        this.name = name;
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }
    //helper constructor makes test easy(because only has one input)
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }

    /**
     * add all coordinates of one ship
     * @param upperLeft point of the ship
     * @param width of the ship
     * @param height of the ship
     * @return the set of coordinates of this ship, which can be regarded as shape
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        int col = upperLeft.getColumn();
        int row = upperLeft.getRow();
        HashSet<Coordinate> shape = new HashSet<>();
        for (int i = 0; i < width; i ++) {
            for (int j = 0; j < height; j ++) {
                shape.add(new Coordinate(row + j, col + i));
            }
        }
        return shape;
    }

    public String getName(){
        return this.name;
    }
}

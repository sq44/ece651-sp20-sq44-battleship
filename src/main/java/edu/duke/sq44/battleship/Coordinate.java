package edu.duke.sq44.battleship;

import java.io.Serializable;

public class Coordinate implements Serializable {
    private final int row;
    private final int column;

    /**
     * Construct the coordinate with integer value of row and column
     * @param r row
     * @param c column
     */
    public Coordinate(int r, int c) {
        if (r < 0 || r > 25) {
            throw new IllegalArgumentException("row of coordinate is out of bound or invalid");
        }
        if (c < 0 || c > 9) {
            throw new IllegalArgumentException("column of coordinate is out of bound or invalid");
        }
        this.row = r;
        this.column = c;
    }

    /**
     * Construct the coordinate with string from user input
     * for row and column and convert to the mapping integer value
     * @param descr user input for coordinate
     */
    public Coordinate(String descr) {
        if (descr.length() != 2) {
            throw new IllegalArgumentException("coordinate " + descr + " has invalid length");
        }
        descr = descr.toUpperCase();
        int r = descr.charAt(0) - 'A';
        int c = descr.charAt(1) - '0';

        if (r < 0 || r > 25) {
            throw new IllegalArgumentException("row of coordinate is out of bound or invalid");
        }
        if (c < 0 || c > 9) {
            throw new IllegalArgumentException("column of coordinate is out of bound or invalid");
        }
        this.row = r;
        this.column = c;
    }


    public int getColumn() {
        return column;
    }
    public int getRow() {
        return row;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(this.getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }
    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}

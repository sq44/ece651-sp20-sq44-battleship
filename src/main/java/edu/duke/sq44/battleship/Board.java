package edu.duke.sq44.battleship;

import java.util.ArrayList;

/**
 * Interface that contain methods of get width and height
 * of the class that implement it
 */
public interface Board<T> {
    public int getWidth();
    public int getHeight();

    /**
     * Add ship to the board
     * @param toAdd is the ship being added
     * @return null or argument that implies invalid placement violet rules
     */
    public String tryAddShip(Ship<T> toAdd);

    /**
     * check the content at given coordinate for this board
     * @param where is the position that is going to be displayed
     * @return display symbol
     */
    public T whatIsAtForSelf(Coordinate where);

    /**
     * check the content at given coordinate for enemy board
     * @param where is the position that is going to be displayed for enemy board
     * @return display symbol
     */
    public T whatIsAtForEnemy(Coordinate where);

    /**
     * do fire at enemy's battleship board
     * @param c player hit at coordinate c
     * @return which ship is hit, if none the null
     */
    public Ship<T> fireAt(Coordinate c);

    /**
     * check whether all the ship on this board is Sunk
     * @return all sunk or not
     */
    public boolean allSunk();

    public ArrayList<Ship<T>> getShips();
}

package edu.duke.sq44.battleship;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class V2ShipFactory implements AbstractShipFactory<Character>{
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        if (where.getOrientation() == 'U' || where.getOrientation() == 'D') throw new IllegalArgumentException("Submarine " +
                "dose not have the: " + where.getOrientation() + " direction");
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        if (where.getOrientation() == 'U' || where.getOrientation() == 'D') throw new IllegalArgumentException("Destroyer " +
                "dose not have the: " + where.getOrientation() + " direction");
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    /**
     * store relative coordinate of valid postion in the smallest box for BatttleShip
     * @param where specifies the location and orientation of the ship to make
     * @return
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        Set<Coordinate> validPos;
        Coordinate c1,c2,c3,c4;
        if (where.getOrientation() == 'U') {
            validPos = drawLocation(new Coordinate(0,1), new Coordinate(1,0),
                    new Coordinate(1,1), new Coordinate(1,2));
        } else if (where.getOrientation() == 'R') {
            validPos = drawLocation(new Coordinate(1,1), new Coordinate(0,0),
                    new Coordinate(1,0), new Coordinate(2,0));
        } else if (where.getOrientation() == 'D'){
            validPos = drawLocation(new Coordinate(1,1), new Coordinate(0,2),
                    new Coordinate(0,1), new Coordinate(0,0));
        } else if (where.getOrientation() == 'L') {
            validPos = drawLocation(new Coordinate(1,0), new Coordinate(2,1),
                    new Coordinate(1,1), new Coordinate(0,1));
        } else {
            throw new IllegalArgumentException("battleShip dose not have the: " + where.getOrientation() + " direction" );
        }
        return where.getOrientation() == 'U' || where.getOrientation() == 'D' ?
                createIrregularShip(where, 3, 2, validPos,'b', "Battleship") : createIrregularShip(where, 2, 3, validPos,'b', "Battleship");
    }

    /**
     *
     * @param where specifies the location and orientation of the ship to make
     * @return
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        Set<Coordinate> validPos;
        Coordinate c1,c2,c3,c4,c5,c6,c7;
        if (where.getOrientation() == 'U') {
            validPos = drawLocation(new Coordinate(0,0), new Coordinate(1,0), new Coordinate(2,0),
                    new Coordinate(3,0), new Coordinate(2,1), new Coordinate(3,1), new Coordinate(4,1));
        } else if (where.getOrientation() == 'R') { //04 03 02 01 12 11 10
            validPos = drawLocation(new Coordinate(0,4), new Coordinate(0,3), new Coordinate(0,2),
                    new Coordinate(0,1), new Coordinate(1,2), new Coordinate(1,1), new Coordinate(1,0));
        } else if (where.getOrientation() == 'D'){ //41 31 21 11 20 10 00
            validPos = drawLocation(new Coordinate(4,1), new Coordinate(3,1), new Coordinate(2,1),
                    new Coordinate(1,1), new Coordinate(2,0), new Coordinate(1,0), new Coordinate(0,0));
        } else if (where.getOrientation() == 'L') { //10 11 12 13 02 03 04
            validPos = drawLocation(new Coordinate(1,0), new Coordinate(1,1), new Coordinate(1,2),
                    new Coordinate(1,3), new Coordinate(0,2), new Coordinate(0,3), new Coordinate(0,4));
        } else {
            throw new IllegalArgumentException("Carrier dose not have the: " + where.getOrientation() + " direction" );
        }
        return where.getOrientation() == 'U' || where.getOrientation() == 'D' ?
                createIrregularShip(where, 2, 5, validPos,'c', "Carrier") : createIrregularShip(where, 5, 2, validPos,'c', "Carrier");
    }

    /**
     * choose points of irregular ship, take in and draw into validPos
     */
    private Set<Coordinate> drawLocation(Coordinate...expectedLocs) {
        HashSet<Coordinate> res = new LinkedHashSet<>();
        for (Coordinate c: expectedLocs) {
            res.add(c);
        }
        return res;
    }

    /**
     * Create four kinds of ships, according to their name, to different shape
     * @param where up left position of ship
     * @param w width of ship
     * @param h height of ship
     * @param letter representing symbol of the ship
     * @param name name of the ship
     * @return
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if(where.getOrientation() == 'H') {
            int temp = h;
            h = w;
            w = temp;
        }
        return new RectangleShip<Character>(name, where.getWhere(), w, h,letter, '*');
    }

    protected Ship<Character> createIrregularShip(Placement where, int w, int h, Set<Coordinate> validPos, char letter, String name) {
        return new IrregularShip<Character>(name, where.getWhere(), w, h,validPos, letter, '*');
    }
}

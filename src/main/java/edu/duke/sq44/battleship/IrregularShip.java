package edu.duke.sq44.battleship;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * class for ship as Irregular,
 * @param <T>
 */
public class IrregularShip<T> extends BasicShip<T> implements Serializable {
    private final String name;
    public IrregularShip(String name, Coordinate upperLeft, int width, int height, Set<Coordinate> validPos, ShipDisplayInfo<T> displayInfoA, ShipDisplayInfo<T> displayInfoB) {
        super(makeCoords(upperLeft, width, height, validPos), displayInfoA, displayInfoB);
        this.name = name;
    }

    public IrregularShip(String name, Coordinate upperLeft, int width, int height, Set<Coordinate> validPos, T data, T onHit) {
        this(name, upperLeft, width, height,validPos, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * add all coordinates of one ship
     * @param upperLeft point of the ship
     * @param width of the ship
     * @param height of the ship
     * @return the set of coordinates of this ship, which can be regarded as shape
     */
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height, Set<Coordinate> validPos) {
        int col = upperLeft.getColumn();
        int row = upperLeft.getRow();
        HashSet<Coordinate> shape = new LinkedHashSet<>(); //in order coordinate
        /*
        for (int i = 0; i < width; i ++) {
            for (int j = 0; j < height; j ++) {
                Coordinate currPos = new Coordinate(row + j, col + i);
                if (!validPos.contains(new Coordinate(j,i))) continue; //relative position (i,j)
                shape.add(currPos);
            }
        }
        */

        for (Coordinate c: validPos) {
            shape.add(new Coordinate(row + c.getRow(), col + c.getColumn()));
        }
        return shape;
    }

    public String getName(){
        return this.name;
    }
}

package edu.duke.sq44.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;

/**
 * Operation for single player interaction in the text view board
 * Containing placement phase and attacking phase
 */
public class TextPlayer implements TextMaster{
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    private final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    int remainSonar;
    int remainMove;

    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputSource;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationList();
        setupShipCreationMap();
        remainMove = 3;
        remainSonar = 3;
    }
    @Override
    public Board<Character> getBoard() {
        return theBoard;
    }

    /**
     * print a prompt and Read the input from inputReader
     * @param prompt print line
     * @return place of the upper left of the ship
     * @throws IOException
     */
    public Placement readPlacement(String prompt) {
        out.println(prompt);
        String s = null;
        Placement p;
        try{
            s = inputReader.readLine();
            p = new Placement(s);
        } catch (Exception e) {
            out.println(e.getMessage());
            p = readPlacement(prompt);//recursion again and get valid result
        }
        if (s == null) throw new IllegalArgumentException("EOF");//no need to handle
        return p;
    }

    /**
     * place all positions of ship on board and display them
     * with upper left position given
     * if the ship is hit, it will be also record in order
     * @throws IOException
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn, Set<Integer> hit) {
        boolean checkValidFormat = false; //check irregular ship with correct orientation
        Ship<Character> s = null;
        Placement p;
        while (!checkValidFormat) {
            p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?"); //check cor valid
            try{ //check ship format with orientation
                s = createFn.apply(p);
                checkValidFormat = true;
                String res = theBoard.tryAddShip(s);
                while (res != null) { //check placement rules
                    out.print(res);
                    p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                    s = createFn.apply(p);
                    res = theBoard.tryAddShip(s);
                }
            } catch(Exception e) {
                out.println(e.getMessage());
            }
        }

        int count = 0;
        for (Coordinate c1: s.getCoordinates()) {
            if (hit.contains(count)) {
                s.recordHitAt(c1);
            }
            count ++;
        }
        out.print(view.displayMyOwnBoard());
    }

    /**
     * This phase is the first phase, player will
     * continue to call doOnePlacement until all ships is placed
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        out.print(view.displayMyOwnBoard());
        out.println("--------------------------------------------------------------------------------\n" +
                "Player " + name +
                ": you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n" +
                "--------------------------------------------------------------------------------");
        Set<Integer> initHitSet = new HashSet<>();
        initHitSet.add(-1); // nothing has been hit
        for (String s: shipsToPlace) {
            Function<Placement, Ship<Character>> createFn = shipCreationFns.get(s);
            doOnePlacement(s, createFn, initHitSet);
        }
    }

    /**
     * current player plays for one turn in attacking phase（fire，move or sonar
     */
    public void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException {
        String bothViews = view.displayMyBoardWithEnemyNextToIt(new BoardTextView(enemyBoard), "Your ocean", enemyName + "'s ocean");
        out.print(bothViews); //firstly show graph to the player
        out.println("Possible actions for Player" + name + ":\n" +
                "\n" +
                " F Fire at a square\n" +
                " M Move a ship to another square (" + remainMove + " remaining)\n" +
                " S Sonar scan (" + remainSonar + " remaining)\n" +
                "\n" +
                "Player " + name + " , what would you like to do?");
        boolean checkValidOption = false;
        while (!checkValidOption) {
            switch (inputReader.readLine()){
                case "F":
                    fireAction(enemyBoard);
                    checkValidOption = true;
                    break;
                case "M":
                    if (remainMove <= 0) {
                        out.println("you cannot move anymore, please choose another option");
                        break; //this time not set the flag to true
                    }
                    moveAction();
                    remainMove --;
                    checkValidOption = true;
                    break;
                case "S":
                    if (remainSonar <= 0) {
                        out.println("you cannot sonar scan anymore, please choose another option");
                        break; //this time not set the flag to true
                    }
                    SonarAction(enemyBoard);
                    remainSonar --;
                    checkValidOption = true;
                    break;
                default:
                    out.println("invalid option,  please choose F,M,or S");
            }
        }
        bothViews = view.displayMyBoardWithEnemyNextToIt(new BoardTextView(enemyBoard), "Your ocean", enemyName + "'s ocean");
        out.print(bothViews);
    }

    /**
     * current player plays for one turn in attacking phase
     */
    public void fireAction(Board<Character> enemyBoard) throws IOException {
        Coordinate p = readPosition("Player " + name + " where do you want to fire at?");
        Ship<Character> target = enemyBoard.fireAt(p);
        if (target == null) {
            out.print("You missed!\n");
        }
        else {
            out.print("you have hit a " + target.getName() + "!\n");
        }
    }

    /**
     * move ship from original coordinate to new coordinate with orientation
     * @throws IOException
     */
    public void moveAction() throws IOException {
        Coordinate c0 = readPosition("Player " + name + " which ship do you want to move?(type the coordinate)");
        for (Ship<Character> targetShip: theBoard.getShips()) {
            for (Coordinate c: targetShip.getCoordinates()) {
                if (c.equals(c0)) {
                    String targetType = targetShip.getName();
                    Set<Integer> hitSet =  targetShip.getHitCoordinate();//record hit pos
                    theBoard.getShips().remove(targetShip);//remove first, because we do not want it overlap new ship
                    doOnePlacement(targetType, shipCreationFns.get(targetType), hitSet);//place new pos
                    //theBoard.getShips().remove(targetShip);
                    return;
                }
            }
        }
        out.println("no valid ship here!please input again");
        moveAction(); //call again
    }

    public void SonarAction(Board<Character> enemyBoard) {
        //name->freq
        HashMap<Character, Integer> mapFreq = new HashMap<>();
        mapFreq.put('s', 0);
        mapFreq.put('d', 0);
        mapFreq.put('b', 0);
        mapFreq.put('c', 0);
        Coordinate sonarCenter = readPosition("Player " + name + " which position do you want to sonar scan at ?(type the coordinate)");
        int r = sonarCenter.getRow();
        int c = sonarCenter.getColumn();

        for (int i = r - 3; i <= r + 3; i ++) {
            for (int j = c - 3; j <= c + 3; j ++) {
                if ( (i == r - 3 && j == c) ||
                        (i == r - 2 && j >= c - 1 && j <= c + 1) ||
                        (i == r + 2 && j >= c - 1 && j <= c + 1) ||
                        (i == r - 1 && j >= c - 2 && j <= c + 2) ||
                        (i == r + 1 && j >= c - 2 && j <= c + 2) ||
                        (i == r)                                 ||
                        (i == r + 3 && j == c)) {
                    if (i < 0 || i >= theBoard.getHeight() || j < 0 || j >= theBoard.getWidth()) continue;;
                    Coordinate curr = new Coordinate(i,j);
                    char sym;
                    if (enemyBoard.whatIsAtForSelf(curr) == null) continue;
                    else sym = enemyBoard.whatIsAtForSelf(curr);
                    if (mapFreq.containsKey(sym)) mapFreq.put(sym, mapFreq.get(sym) + 1);
                }
            }
        }
        out.println("---------------------------------------------------------------------------\n" +
                "Submarines occupy " + mapFreq.get('s') + " squares\n" +
                "Destroyers occupy " + mapFreq.get('d') + " squares\n" +
                "Battleships occupy " + mapFreq.get('b') + " squares\n" +
                "Carriers occupy " + mapFreq.get('c') + " square\n" +
                "---------------------------------------------------------------------------");
    }


    /**
     * Read the position from line and get coordinate, similar to readPlacement(I changed
     * readFirPositio into this more general function
     * @param prompt for display
     * @return coordinate for fire
     * @throws IOException
     */
    public Coordinate readPosition(String prompt) {
        out.println(prompt);
        Coordinate c;
        String s = null;
        try{
            s = inputReader.readLine();
            c = new Coordinate(s);
        } catch (Exception e) {
            out.println(e.getMessage());
            c = readPosition(prompt);//recursion again and get valid result
        }
        if (s == null) throw new IllegalArgumentException("EOF");
        return c;
    }

    /**
     * check whether this player has win or not
     * @return result of check
     */
    public boolean checkWin(Board<Character> enemyBoard) {
        if (enemyBoard.allSunk()) {
            out.println("Player " + name + " win!");
            return true;
        }
        return false;//doesn't meet ending condition
    }

    /* helper method for create ships to their mapping functions */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> (shipFactory.makeBattleship(p)));
        shipCreationFns.put("Carrier", (p) -> (shipFactory.makeCarrier(p)));
    }

    /* helper method for create ships in storage list */
    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }
}

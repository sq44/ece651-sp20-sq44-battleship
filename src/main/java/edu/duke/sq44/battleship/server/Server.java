package edu.duke.sq44.battleship.server;

import edu.duke.sq44.battleship.Board;
import edu.duke.sq44.battleship.BoardTextView;
import edu.duke.sq44.battleship.PlayerMsg;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * game server for the battleship game, now can serve two players at the same time, and the game will switch and
 * play for two players turn by turn the info will be updated and send to next player every half turn
 */
public class Server {
    private Map<String, Socket> playerSockets;
    private Map<String, Board<Character>> playerBoards;
    Map<String, ObjectInputStream> readers;
    Map<String, ObjectOutputStream> writers;
    PlayerMsg msg;

    public Server() {
        playerSockets = new HashMap<>();
        playerBoards = new HashMap<>();
        readers = new HashMap<>();
        writers = new HashMap<>();
    }
    //allowing two players play at the same time one by one and turn by turn
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Server server = new Server();
        ServerSocket sSocket = new ServerSocket(8888);
        int clientCount = 0;
        while (clientCount < 2) {
            Socket s = sSocket.accept();
            ObjectInputStream in = new ObjectInputStream(s.getInputStream());
            ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
            server.msg = (PlayerMsg) in.readObject();
            String name = server.msg.getClientName();
            Board<Character> b = server.msg.getTheBoard();
            server.playerSockets.put(name, s);
            server.playerBoards.put(name, b);
            server.readers.put(name, in);
            server.writers.put(name, out);
            clientCount ++;
        }
        server.playGame();
    }

    /**
     * manage logic of game play part in server, it will
     * record player name, socket and board info,
     * the game will start and then going to the placement phase and
     * the to the attack phase.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void playGame() throws IOException, ClassNotFoundException {
        System.out.println("Game Start!");
        //placing phase
        for (String p: writers.keySet()) { //sending
            writers.get(p).writeObject(new PlayerMsg(null, null,false, null, true, false));
        }
        for (String p: readers.keySet()) { //receiving
            ObjectInputStream in = readers.get(p);
            msg = (PlayerMsg) in.readObject();
            playerBoards.put(p, msg.getTheBoard()); //update board
        }
        for (Board<Character> b: playerBoards.values()) { //check maps in the beginning
            System.out.println(new BoardTextView(b).displayMyOwnBoard());
        }

        //attacking phase
        String[] attackers = new String[2];
        int idx = 0;
        for (String p: writers.keySet()) {
            attackers[idx] = p;
            idx ++;
        }
        while (!msg.isHasWin()) { //send enemy board to player, receive players board, send it to next enemy
            writers.get(attackers[0]).writeObject(new PlayerMsg(playerBoards.get(attackers[0]), playerBoards.get(attackers[1]), false, null, false, true));
            msg = (PlayerMsg) readers.get(attackers[0]).readObject();
            playerBoards.put(attackers[0], msg.getTheBoard());
            playerBoards.put(attackers[1], msg.getEnemyBoard());

            writers.get(attackers[1]).writeObject(new PlayerMsg(playerBoards.get(attackers[1]), playerBoards.get(attackers[0]),false, null, false, true));
            msg = (PlayerMsg) readers.get(attackers[1]).readObject();
            playerBoards.put(attackers[0], msg.getEnemyBoard());
            playerBoards.put(attackers[1], msg.getTheBoard());
        }
    }
}

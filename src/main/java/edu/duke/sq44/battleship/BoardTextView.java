package edu.duke.sq44.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of a Board
 * (i.e., converting it to a string shown to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the enemy's board.
 */
public class BoardTextView {
    private final  Board<Character> toDisplay;
    /**
     * Constructs a BoardView, given the board it will display.
     * @param toDisplay is the Board to display
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    public <T> BoardTextView(BattleShipBoard<T> tBattleShipBoard) {
        toDisplay = (BattleShipBoard<Character>)tBattleShipBoard;
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     *
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep = ""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = " | ";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * Make the string used to display on terminal, for mine or enemy's board
     * with header and body strings being combined
     * @return the string used for display
     */
    public String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String displayResult = "";
        String seg = "   |";
        String[] alpha = new String[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String header = makeHeader();
        displayResult += header;

        for (int i = 0; i < toDisplay.getHeight(); i ++) {
            displayResult += alpha[i];
            if (toDisplay.getWidth() == 1) {
                if (getSquareFn.apply(new Coordinate(i,0)) == null)  {
                    seg = "   ";
                } else {
                    seg = " " + getSquareFn.apply(new Coordinate(i,0)) + " ";
                }
                displayResult += seg + alpha[i] + "\n";
                continue;
            }
            //interval with '|'
            for (int j = 0; j < toDisplay.getWidth() - 1; j ++) {
                Coordinate c = new Coordinate(i, j);
                if (getSquareFn.apply(c) == null)  {
                    seg = "   |";
                } else {
                    seg = " " + getSquareFn.apply(c) + " |";
                }
                displayResult += seg;
            }
            //last one without '|'
            if (getSquareFn.apply(new Coordinate(i,toDisplay.getWidth() - 1)) == null)  {
                seg = "   ";
            } else {
                seg = " " + getSquareFn.apply(new Coordinate(i,toDisplay.getWidth() - 1)) + " ";
            }
            displayResult += seg;
            displayResult += alpha[i];
            displayResult += "\n";
        }
        displayResult += header;
        return displayResult; //this is a placeholder for the moment
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    /**
     * Display my board with enemy's board in same zone, next to each other
     * @param enemyView board view for enemy
     * @param myHeader board view header for current player(my)
     * @param enemyHeader board view header for enemy
     * @return combined result string that can show both in same zone
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        //I modify the board to be good-looking, so distance is different(4W + 11);
        String[] myBoardView = this.displayMyOwnBoard().split("\n");
        String[] enemyBoardView = enemyView.displayEnemyBoard().split("\n");
        String spaceJump = "";
        for (int i = 0; i < 4 * toDisplay.getWidth() + 11; i ++) spaceJump += " ";
        StringBuilder sb = new StringBuilder();

        sb.append(myHeader);
        sb.append(spaceJump);
        sb.append(enemyHeader + "\n");

        sb.append(myBoardView[0] + "                      " + enemyBoardView[0] + "\n"); //22 chars
        for (int i = 1; i < myBoardView.length - 1; i ++) {
            sb.append(myBoardView[i]);
            sb.append("                    "); //two space less than header, 20 chars
            sb.append(enemyBoardView[i] + "\n");
        }
        sb.append(myBoardView[myBoardView.length - 1] + "                      " + enemyBoardView[myBoardView.length - 1] + "\n");//22 chars

        return sb.toString();
    }

}

package edu.duke.sq44.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character>{

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");//default is vertical
    }

    /**
     * Create four kinds of ships, according to their name, to different shape
     * @param where up left position of ship
     * @param w width of ship
     * @param h height of ship
     * @param letter representing symbol of the ship
     * @param name name of the ship
     * @return
     */
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        if(where.getOrientation() == 'H') {
            int temp = h;
            h = w;
            w = temp;
        }
        return new RectangleShip<Character>(name, where.getWhere(), w, h,letter, '*');
    }
}

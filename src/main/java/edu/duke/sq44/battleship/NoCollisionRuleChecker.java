package edu.duke.sq44.battleship;

import java.io.Serializable;

/**
 * check whether the ship to be placed has overlap collision with any existing ships
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> implements Serializable {
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> coorSet = theShip.getCoordinates();
        for (Coordinate c: coorSet) {
            if (theBoard.whatIsAtForSelf(c) != null) return "That placement is invalid: the ship overlaps another ship.\n";
        }
        return null;
    }

}

package edu.duke.sq44.battleship;

/**
 * Interface Used for display
 * @param <T>
 */
public interface ShipDisplayInfo<T> {
    public T getInfo(Coordinate where, boolean hit);
}

package edu.duke.sq44.battleship;

import java.io.IOException;
import java.util.Set;
import java.util.function.Function;

/**
 * master for the player and computer
 */
public interface TextMaster {
    void doPlacementPhase() throws IOException;
    void playOneTurn(Board<Character> enemyBoard, String enemyName) throws IOException;
    boolean checkWin(Board<Character> enemyBoard);
    Board<Character> getBoard();
}

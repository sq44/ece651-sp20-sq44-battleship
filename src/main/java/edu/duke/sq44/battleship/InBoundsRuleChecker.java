package edu.duke.sq44.battleship;

import java.io.Serializable;

/**
 * check whether the ship being placed is in the range of board
 * while I have not check case for row or col < 0, because the validness
 * of coordinate must be checked when being created, so check here is unnecessary
 */
public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> implements Serializable {

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> coorSet = theShip.getCoordinates();
        for (Coordinate c: coorSet) {
            if (c.getColumn() >= theBoard.getWidth()) {
                return "That placement is invalid: the ship goes off the right of the board.\n";
            }
            if (c.getRow() >= theBoard.getHeight()) {
                return "That placement is invalid: the ship goes off the bottom of the board.\n";
            }
        }
        return null;
    }

}

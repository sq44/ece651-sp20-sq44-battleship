package edu.duke.sq44.battleship;

import java.io.Serializable;
import java.util.*;

/**
 * Abstract class implement ship and the parent of RectangleShip
 * @param <T>
 */
public abstract class BasicShip<T> implements Ship<T>, Serializable {
    protected Map<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        myPieces = new LinkedHashMap<>();
        for (Coordinate c: where) {
            myPieces.put(c, false);
        }
    }

    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }

    @Override
    public boolean isSunk() {
        for (Map.Entry<Coordinate, Boolean> entry : myPieces.entrySet()) {
            if (entry.getValue() == false) return false;
        }
            return true;
    }

    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        if (myPieces.get(where) == true) return true;
        return false;
    }

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip){
        return myShip ? myDisplayInfo.getInfo(where, wasHitAt(where)) : enemyDisplayInfo.getInfo(where, wasHitAt(where));
    }

    protected void checkCoordinateInThisShip(Coordinate c) {
        if (myPieces.containsKey(c)) return;
        throw new IllegalArgumentException("Coordinate " + c + "  is not part of this ship");
    }

    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    @Override
    public Set<Integer> getHitCoordinate() {
        Set<Integer> order = new LinkedHashSet<>(); //in insert order
        int i = 0;
        for (Map.Entry<Coordinate, Boolean> entry : myPieces.entrySet()) {
            if (entry.getValue() == true)  order.add(i);
            i ++;
        }
        return order;
    }
}

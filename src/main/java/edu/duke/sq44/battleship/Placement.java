package edu.duke.sq44.battleship;

public class Placement {
    private final Coordinate where;
    private final char orientation;

    /**
     * Construct placement for user input given two para
     * @param where is the coordinate of left and up most of ship
     * @param orientation is the laying rule of ship(vertical or horizontal)
     */
    public Placement(Coordinate where, char orientation) {
        if (orientation == 'v' || orientation == 'h' ||
                orientation == 'u'|| orientation == 'r' || orientation == 'd' || orientation == 'l') {
            orientation -= 32;
        }
        if (orientation != 'V' && orientation != 'H' &&
                orientation != 'U' && orientation != 'R' && orientation != 'D' && orientation != 'L') {
            throw new IllegalArgumentException("orientation is invalid");
        }
        this.where = where;
        this.orientation = orientation;
    }

    /**
     * Construct placement given one pare containing all info
     * @param place first two char is the coordinate and last char is orientation
     */
    public Placement(String place) {
        this(new Coordinate(place.substring(0,2)), place.charAt(2));
        if (place.length() != 3) {
            throw new IllegalArgumentException("place " + place + " has invalid length");
        }
    }
    /* get position and orientation */
    public Coordinate getWhere() {
        return where;
    }
    public char getOrientation() {
        return orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(this.getClass())) {
            Placement p = (Placement) o;
            return where.equals(p.getWhere()) && orientation == p.getOrientation();
        }
        return false;
    }
    @Override
    public String toString() {
        return where + Character.toString(orientation);
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}

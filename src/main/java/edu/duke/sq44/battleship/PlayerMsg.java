package edu.duke.sq44.battleship;

import java.io.Serializable;

/**
 * this class is the message packet
 * containing information that will be passed between the player/client and the server
 * it contains the self board and enemy's board and signal that will help
 * the client and server to tell the stage currently.
 */
public class PlayerMsg implements Serializable {
    final boolean placeSignal;
    final boolean attackSignal;
    final Board<Character> theBoard;
    final Board<Character> enemyBoard;
    final boolean hasWin;
    final String clientName;

    public PlayerMsg(Board<Character> theBoard, Board<Character> enemyBoard, boolean hasWin, String clientName, boolean placeSignal, boolean attackSignal) {
        this.theBoard = theBoard;
        this.enemyBoard = enemyBoard;
        this.hasWin = hasWin;
        this.clientName = clientName;
        this.placeSignal = placeSignal;
        this.attackSignal = attackSignal;
    }

    public Board<Character> getTheBoard() {
        return theBoard;
    }

    public Board<Character> getEnemyBoard() {
        return enemyBoard;
    }

    public boolean isHasWin() {
        return hasWin;
    }

    public String getClientName() {
        return clientName;
    }

    public boolean isPlaceSignal() {
        return placeSignal;
    }

    public boolean isAttackSignal() {
        return attackSignal;
    }
    @Override
    public String toString() {
        return "MSG:\n" + (new BoardTextView(theBoard).displayMyOwnBoard()) + "\n"
                + clientName + "\n"
                + "A: " + attackSignal + " P: " + placeSignal + "\n";
    }
}

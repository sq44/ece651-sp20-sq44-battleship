package edu.duke.sq44.battleship;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * this class is the network version of pvp battleship, based on socket
 * containing read, place and fire operation
 * check win or lose condition
 */
public class RemoteApp {
    final TextMaster player;
    final String clientName;
    ObjectOutputStream writer;
    ObjectInputStream reader;
    Socket s;
    PlayerMsg msg;

    public RemoteApp(TextMaster player, String clientName, Socket s) {
        this.player = player;
        this.clientName = clientName;
        this.s = s;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        onlineGame(args[0], args[1], Integer.parseInt(args[2])); //todo: add name
    }

    public static void onlineGame(String selfName, String hostName, int port) throws IOException, ClassNotFoundException {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X'); //send to server socket
        Socket s = new Socket(hostName, port);
        Board<Character> b2; //todo:receive from server socket
        V2ShipFactory factory = new V2ShipFactory();
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        TextPlayer p = new TextPlayer("A", b1, input, System.out, factory);
        //InputStream cpStream = App.class.getClassLoader().getResourceAsStream("input.txt");
        //BufferedReader input2 = new BufferedReader(new InputStreamReader(cpStream));
        //TextPlayer p = new TextPlayer("A", b1, input, System.out, factory);

        RemoteApp app = new RemoteApp(p, selfName, s);
        app.writer = new ObjectOutputStream(s.getOutputStream());
        app.reader = new ObjectInputStream(s.getInputStream());
        app.msg = new PlayerMsg(b1, null,false, selfName, false, false);
        app.writer.writeObject(app.msg);
        app.writer.flush();
        app.writer.reset();

        app.msg = (PlayerMsg) app.reader.readObject();
        if (app.msg.placeSignal) System.out.println("begin placement phase");
        app.doPlacementPhase(); //todo: add waiting for another player
        app.doAttackingPhase();
    }

    /**
     * Placement phase for both players, one for 10 turns
     * @throws IOException
     */
    public void doPlacementPhase() throws IOException {
        player.doPlacementPhase();
        msg = new PlayerMsg(player.getBoard(), null, false, clientName, false, false);
        System.out.println(msg);
        writer.writeObject(msg);
    }

    /**
     * attacking phase for both players,one for one turn until one player win
     * @throws IOException
     */
    public void doAttackingPhase() throws IOException, ClassNotFoundException {
        System.out.println("attack will start......");
        while (!msg.hasWin) { //two-way check: signal from server / win condition local
            msg = (PlayerMsg) reader.readObject();
            player.playOneTurn(msg.enemyBoard, "Player B");
            if (player.checkWin(msg.theBoard)) {
                writer.writeObject(new PlayerMsg(player.getBoard(), msg.enemyBoard, true, clientName, false, false));
                return;
            }
            writer.writeObject(new PlayerMsg(player.getBoard(), msg.enemyBoard, false, clientName, false, false));
        }
    }
}

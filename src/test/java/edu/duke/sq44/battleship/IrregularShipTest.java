package edu.duke.sq44.battleship;

import java.util.HashSet;

import static edu.duke.sq44.battleship.RectangleShip.makeCoords;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import static edu.duke.sq44.battleship.IrregularShip.makeCoords;
import static org.junit.jupiter.api.Assertions.*;
public class IrregularShipTest {
    @Test
    public void test_make_cords(){
        Coordinate c1 = new Coordinate(1,1);
        Coordinate c1R = new Coordinate(0,0);//(0,0) is the relative pos for (1,0)
        Coordinate c3 = new Coordinate(1,3);
        Coordinate c3R = new Coordinate(0,2);
        HashSet<Coordinate> valid = new HashSet<>();
        valid.add(c1R);
        valid.add(c3R);
        HashSet<Coordinate> actual = makeCoords(c1, 3,1, valid);
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(c1);
        expected.add(c3);
        assertEquals(expected, actual);
    }


}

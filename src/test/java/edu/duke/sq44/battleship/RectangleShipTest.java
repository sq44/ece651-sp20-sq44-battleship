package edu.duke.sq44.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static edu.duke.sq44.battleship.RectangleShip.makeCoords;

public class RectangleShipTest {

    @Test
    public void test_make_cords(){
        Coordinate c1 = new Coordinate(1,1);
        HashSet<Coordinate> actual = makeCoords(c1, 3,1);
        Coordinate c2 = new Coordinate(1,2);
        Coordinate c3 = new Coordinate(1,3);
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(c1);
        expected.add(c2);
        expected.add(c3);
        assertEquals(expected, actual);
    }

    @Test
    public void test_occupiesCoordinates() {
        Coordinate upperLeft = new Coordinate(2,3);
        RectangleShip<Character> recs = new RectangleShip<>("submarine", upperLeft, 5,2, 's', '*');
        Coordinate c2 = new Coordinate(2,4);
        Coordinate c3 = new Coordinate(2,5);
        Coordinate c4 = new Coordinate(2,6);
        Coordinate c5 = new Coordinate(2,7);
        assertEquals(true, recs.occupiesCoordinates(upperLeft));
        assertEquals(true, recs.occupiesCoordinates(c2));
        assertEquals(true, recs.occupiesCoordinates(c3));
        assertEquals(true, recs.occupiesCoordinates(c4));
        assertEquals(true, recs.occupiesCoordinates(c5));
        assertEquals(false, recs.occupiesCoordinates(new Coordinate(2,8)));
        assertEquals(false, recs.occupiesCoordinates(new Coordinate(2,2)));
        Coordinate c6 = new Coordinate(3,7);
        assertEquals(true, recs.occupiesCoordinates(c6));
    }

    @Test
    public void test_basic_ship() {
        Coordinate upperLeft = new Coordinate(2,3);
        RectangleShip<Character> recs = new RectangleShip<>("submarine", upperLeft, 4,1, 's', '*');
        Coordinate h1 = new Coordinate(2,4);
        Coordinate h2 = new Coordinate(2,5);
        Coordinate h3 = new Coordinate(2,6);
        Coordinate h4 = new Coordinate(2,3);
        recs.recordHitAt(h1);
        recs.recordHitAt(h1);
        recs.recordHitAt(h2);
        recs.recordHitAt(h3);
        assertEquals(true, recs.wasHitAt(h1));
        assertEquals(true, recs.wasHitAt(h2));
        assertEquals(true, recs.wasHitAt(h3));
        assertEquals(false, recs.wasHitAt(h4));
        assertEquals(false, recs.isSunk());
        assertThrows(IllegalArgumentException.class, () -> recs.wasHitAt(new Coordinate(1,1)));
        assertThrows(IllegalArgumentException.class, () -> recs.recordHitAt(new Coordinate(2,7)));

        recs.recordHitAt(h4);
        assertEquals(true, recs.wasHitAt(h4));
        assertEquals(true, recs.isSunk());
    }

    @Test
    public void test_basic_ship_display() {
        Coordinate upperLeft = new Coordinate(2,3);
        Ship<Character> s = new RectangleShip<Character>("submarine", upperLeft, 4,1, 's', '*');
        Coordinate h1 = new Coordinate(2,4);
        Coordinate h2 = new Coordinate(2,5);
        Coordinate h3 = new Coordinate(2,6);

        assertEquals('s', s.getDisplayInfoAt(upperLeft,true));
        /* check for same ship which shown to enemy */
        assertEquals(null, s.getDisplayInfoAt(upperLeft,false));

        assertEquals('s', s.getDisplayInfoAt(h1,true));
        assertEquals(null, s.getDisplayInfoAt(upperLeft,false));

        assertEquals('s', s.getDisplayInfoAt(h2,true));
        assertEquals(null, s.getDisplayInfoAt(upperLeft,false));

        assertEquals('s', s.getDisplayInfoAt(h3,true));
        assertEquals(null, s.getDisplayInfoAt(upperLeft,false));

        s.recordHitAt(upperLeft);
        assertEquals('*', s.getDisplayInfoAt(upperLeft,true));
        s.recordHitAt(h1);
        assertEquals('*', s.getDisplayInfoAt(h1,true));
        s.recordHitAt(h1);//repeat test

        s.recordHitAt(h2);
        s.recordHitAt(h3);
        assertEquals('*', s.getDisplayInfoAt(h1,true));
        assertEquals('*', s.getDisplayInfoAt(h2,true));
        assertEquals('*', s.getDisplayInfoAt(h3,true));

        /* check for same ship which shown to enemy */
        assertEquals('s', s.getDisplayInfoAt(upperLeft,false));
        assertEquals('s', s.getDisplayInfoAt(h1,false));
        assertEquals('s', s.getDisplayInfoAt(h2,false));
        assertEquals('s', s.getDisplayInfoAt(h3,false));
    }

    @Test
    public void test_get_name() {
        Coordinate upperLeft = new Coordinate(2,3);
        Ship<Character> s = new RectangleShip<Character>("submarine", upperLeft, 4,1, 's', '*');
        assertEquals("submarine", s.getName());
        assertNotEquals("sub", s.getName());
    }

    @Test
    public void test_get_coor() {
        Coordinate upperLeft = new Coordinate(2,3);
        Ship<Character> s = new RectangleShip<Character>("submarine", upperLeft, 4,1, 's', '*');
        Coordinate h1 = new Coordinate(2,4);
        Coordinate h2 = new Coordinate(2,5);
        Coordinate h3 = new Coordinate(2,6);

        Iterable<Coordinate> cSet = s.getCoordinates();
        for (Coordinate c: cSet) {
            assert(c.equals(h1) || c.equals(h2) || c.equals(h3) || c.equals(upperLeft));
        }
    }
}

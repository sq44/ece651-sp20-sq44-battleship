package edu.duke.sq44.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class InBoundsRuleCheckerTest {
    @Test
    public void test_place_rule() {
        InBoundsRuleChecker<Character> bRule = new InBoundsRuleChecker<>(null);
        BattleShipBoard<Character> board = new BattleShipBoard<>(4,4, 'X',bRule);
        V1ShipFactory v = new V1ShipFactory();
        Ship<Character> des = v.makeDestroyer(new Placement("A7h"));

        assertEquals("That placement is invalid: the ship goes off the right of the board.\n",
                bRule.checkPlacement(des, board));

        Ship<Character> car = v.makeCarrier(new Placement("E0h"));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.\n",
                bRule.checkPlacement(car, board));

        Ship<Character> bat = v.makeBattleship(new Placement("B1V"));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.\n",
                bRule.checkPlacement(bat, board));

        Ship<Character> sub = v.makeSubmarine(new Placement("b2h"));
        assertEquals(null, bRule.checkPlacement(sub, board));
    }
}

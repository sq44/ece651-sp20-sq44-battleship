package edu.duke.sq44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;

public class AppTest {
    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_main_pvp() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output.txt");
        assertNotNull(expectedStream);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            //App.main(new String[0]);
            App.pvpMode();
        }
        finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }

        String expected = new String(expectedStream.readAllBytes()).replace("\r","");
        String actual = bytes.toString().replace("\r",""); //CF -> LF
        assertEquals(expected, actual);
    }
    @Test
    void test_main_pve() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output_pve.txt");
        assertNotNull(expectedStream);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            App.pveMode();
        }
        finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }

        String expected = new String(expectedStream.readAllBytes()).replace("\r","");
        String actual = bytes.toString().replace("\r",""); //CF -> LF
        assertEquals(expected, actual);
    }
    @Test
    void test_main_evp() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output_evp.txt");
        assertNotNull(expectedStream);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            App.evpMode();
        }
        finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }

        String expected = new String(expectedStream.readAllBytes()).replace("\r","");
        String actual = bytes.toString().replace("\r",""); //CF -> LF
        assertEquals(expected, actual);
    }
    @Test
    void test_main_eve() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream input = getClass().getClassLoader().getResourceAsStream("inputWithSM.txt");
        assertNotNull(input);
        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output_eve.txt");
        assertNotNull(expectedStream);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            App.eveMode();
        }
        finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }

        String expected = new String(expectedStream.readAllBytes()).replace("\r","");
        String actual = bytes.toString().replace("\r",""); //CF -> LF
        assertEquals(expected, actual);
    }

    //@Test
    public void test_do_placement_and_attacking_phase_win() throws IOException {
        StringReader sr = new StringReader("a5H\nb5H\nC5H\nd5H\ne5H\nf5H\ng5H\nh0H\nh3h\nI0H\nz0h\nj0h\n" + //z is used to check out of place bound print out, h overlap
                "a5H\nb5H\nC5H\nd5H\ne5H\nf5H\ng5H\nh0H\nI0H\nj0h\n" +
                "a5\na5\na6\na6\nb5\nb5\nb6\nb6\n" + //fire all submarine
                "c5\nc5\nc6\nc6\nc7\nc7\n" +
                "d5\nd5\nd6\nd6\nd7\nd7\n" +
                "k5\nl5\nm6\nn6\no7\np7\n" + //both miss sth
                "e5\ne5\ne6\ne6\ne7\ne7\n" + //fire all destroy
                "f5\nf5\nf6\nf6\nf7\nf7\nf8\nf8\n" +
                "g5\ng5\ng6\ng6\ng7\ng7\ng8\ng8\n" +
                "h0\nh0\nh1\nh1\nh2\nh2\nh3\nh3\n" + //fire all battle
                "i0\ni0\ni1\ni1\ni2\ni2\ni3\ni3\ni4\ni4\ni5\ni5\n" +
                "j0\nj0\nj1\nj1\nj2\nj2\nj3\nj3\nj4\nj4\nz5\nj5\nj5\n"); //z is used to check out of fire bound print out
                Board<Character> b1 = new BattleShipBoard<Character>(9, 15,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(9, 15,'X');
        BufferedReader input = new BufferedReader(sr);
        V1ShipFactory factory = new V1ShipFactory();
        TextPlayer p1 = new TextPlayer("A", b1, input, System.out, factory);
        TextPlayer p2 = new TextPlayer("B", b2, input, System.out, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }

    //@Test
    public void test_do_placement_and_attacking_phase_lose() throws IOException {
        StringReader sr = new StringReader("a5H\nb5H\nC5H\nd5H\ne5H\nf5H\ng5H\nh0H\nh3h\nI0H\nz0h\nj0h\n" + //z is used to check out of place bound print out, h overlap
                "a5H\nb5H\nC5H\nd5H\ne5H\nf5H\ng5H\nh0H\nI0H\nj0h\n" +
                "F\na5\nF\na5\nF\na6\nF\na6\nF\nb5\nF\nb5\nF\nb6\nF\nb6\n" + //fire all submarine
                "F\nc5\nF\nc5\nF\nc6\nF\nc6\nF\nc7\nF\nc7\n" +
                "F\nd5\nF\nd5\nF\nd6\nF\nd6\nF\nd7\nF\nd7\n" +
                "F\nk5\nF\nl5\nF\nm6\nF\nn6\nF\no7\nF\np7\n" + //both miss sth
                "F\ne5\nF\ne5\nF\ne6\nF\ne6\nF\ne7\nF\ne7\n" + //fire all destroy
                "F\nf5\nF\nf5\nF\nf6\nF\nf6\nF\nf7\nF\nf7\nF\nf8\nF\nf8\n" +
                "F\ng5\nF\ng5\nF\ng6\nF\ng6\nF\ng7\nF\ng7\nF\ng8\nF\ng8\n" +
                "F\nh0\nF\nh0\nF\nh1\nF\nh1\nF\nh2\nF\nh2\nF\nh3\nF\nh3\n" + //fire all battle
                "F\ni0\nF\ni0\nF\ni1\nF\ni1\nF\ni2\nF\ni2\nF\ni3\nF\ni3\nF\ni4\nF\ni4\nF\ni5\nF\ni5\n" +
                "F\nj0\nF\nj0\nF\nj1\nF\nj1\nF\nj2\nF\nj2\nF\nj3\nF\nj3\nF\nj4\nF\nj4\nF\nz5\nF\nj5\nF\nj5\n"); //z is used to check out of fire bound print out
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20,'X');
        BufferedReader input = new BufferedReader(sr);
        V1ShipFactory factory = new V1ShipFactory();
        TextPlayer p1 = new TextPlayer("A", b1, input, System.out, factory);
        TextPlayer p2 = new TextPlayer("B", b2, input, System.out, factory);
        App app = new App(p1, p2);
        app.doPlacementPhase();
        app.doAttackingPhase();
    }
}

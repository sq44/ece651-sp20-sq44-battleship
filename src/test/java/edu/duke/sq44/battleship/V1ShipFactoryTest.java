package edu.duke.sq44.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class V1ShipFactoryTest {
    @Test
    public void test_create_ship() {
        V1ShipFactory v = new V1ShipFactory();
        Placement p1 = new Placement("A5V");
        Ship<Character> sub = v.makeSubmarine(p1);
        checkShip(sub, "Submarine", 's', new Coordinate(0,5), new Coordinate(1,5));

        Placement p2 = new Placement("C0h");
        Ship<Character> bat = v.makeBattleship(p2);
        checkShip(bat,"Battleship", 'b', new Coordinate(2,0), new Coordinate(2,1),
                new Coordinate(2,2), new Coordinate(2, 3));

        Placement p3 = new Placement("B9V");
        Ship<Character> car = v.makeCarrier(p3);
        checkShip(car,"Carrier", 'c', new Coordinate(1, 9), new Coordinate(2,9), new Coordinate(3,9),
        new Coordinate(4,9), new Coordinate(5,9), new Coordinate(6,9));

        Placement  p4 = new Placement("T5H");
        Ship<Character> des = v.makeDestroyer(p4);
        checkShip(des, "Destroyer", 'd', new Coordinate(19, 5), new Coordinate(19, 6), new Coordinate(19,7));

    }

    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c: expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c,true));
        }
    }

}

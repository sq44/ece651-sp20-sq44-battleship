package edu.duke.sq44.battleship;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.Test;

public class BattleShipBoardTest{
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(-8, 20,'X'));
    }

    @Test
    public void test_ship_coordinates() {
        BattleShipBoard<Character> b = new BattleShipBoard<>(2,2,'X');
        Character[][] expected = {{null, null}, {null, null}};
        checkWhatIsAtBoard(b, expected); //empty board check
        /* self place check */
        for (int i = 0; i < expected.length; i ++) {
            for (int j = 0; j < expected[0].length; j ++) {
                Coordinate c = new Coordinate(i, j);
                RectangleShip<Character> s = new RectangleShip<Character>(c, 's', '*');
                b.tryAddShip(s);
                expected[i][j] = 's';
                checkWhatIsAtBoard(b, expected);
            }
        }
        /* hit enemy check */
        BattleShipBoard<Character> enemyBoard = new BattleShipBoard<>(2,2,'X');
        Coordinate c1 = new Coordinate(0, 0);
        RectangleShip<Character> s1 = new RectangleShip<Character>(c1, 's', '*');
        Coordinate c2 = new Coordinate(0, 1);
        RectangleShip<Character> s2 = new RectangleShip<Character>(c2, 's', '*');
        assertEquals(null, enemyBoard.whatIsAtForEnemy(c1));//before add
        enemyBoard.tryAddShip(s1);
        enemyBoard.tryAddShip(s2);
        assertEquals(null, enemyBoard.whatIsAtForEnemy(c1));//before hit
        enemyBoard.fireAt(c1);
        assertEquals('s', enemyBoard.whatIsAtForEnemy(c1));//after hit
        assertEquals(null, enemyBoard.whatIsAtForEnemy(c2));//before hit
        enemyBoard.fireAt(c2);
        assertEquals('s', enemyBoard.whatIsAtForEnemy(c2));//after hit
        Coordinate c3 = new Coordinate(1,1);
        enemyBoard.fireAt(c3);//miss hit
        assertEquals('X', enemyBoard.whatIsAtForEnemy(c3));
    }

    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
        for (int i = 0; i < expected.length; i ++) {
            for (int j = 0; j < expected[0].length; j ++) {
                Coordinate temp = new Coordinate(i, j);
                assertEquals(b.whatIsAtForSelf(temp), expected[i][j]);
            }
        }
    }

    @Test
    public void test_try_add_with_placement_rule() {
        Board<Character> b = new BattleShipBoard<Character>(10,10, 'X');
        V1ShipFactory v = new V1ShipFactory();
        Ship<Character> car = v.makeCarrier(new Placement("e4v"));
        assertEquals(null, b.tryAddShip(car));
        assertEquals("That placement is invalid: the ship overlaps another ship.\n", b.tryAddShip(car));
        Ship<Character> des = v.makeDestroyer(new Placement("F3H"));
        assertEquals("That placement is invalid: the ship overlaps another ship.\n", b.tryAddShip(des));
    }

    @Test
    public void test_fire_at() {
        Board<Character> b = new BattleShipBoard<Character>(10,10,'X');
        V1ShipFactory v = new V1ShipFactory();
        Ship<Character> car = v.makeCarrier(new Placement("e4v"));
        b.tryAddShip(car);
        assertSame(car, b.fireAt(new Coordinate("e4")));
        assertEquals(null, b.fireAt(new Coordinate("a0")));

        b.fireAt(new Coordinate("f4"));
        assertEquals(false, car.isSunk());
        b.fireAt(new Coordinate("g4"));
        b.fireAt(new Coordinate("h4"));
        b.fireAt(new Coordinate("i4"));
        //b.fireAt(new Coordinate("j4"));
        b.fireAt(new Coordinate(9,4));
        assertEquals(true, car.isSunk());
    }
}

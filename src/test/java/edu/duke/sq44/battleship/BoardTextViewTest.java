package edu.duke.sq44.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard(11,20,'X');
        Board<Character> tallBoard = new BattleShipBoard(10,27,'X');
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
        //you should write two assertThrows here
    }

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard(w, h,'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    private void notEmptyBoardHelper(String expectedHeader, String expectedBody, Board b1){
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    private void emptyEnemyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard(w, h,'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayEnemyBoard());
    }

    private void notEmptyEnemyBoardHelper(String expectedHeader, String expectedBody, Board b1){
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayEnemyBoard());
    }

    @Test
    public void test_display_empty_1by1() {
        String expectedHeader= "  0\n";
        String expectedBody =
                "A   A\n";
        emptyBoardHelper(1, 1, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_1by2() {
        String expectedHeader= "  0\n";
        String expectedBody =
                "A   A\n" +
                        "B   B\n";
        emptyBoardHelper(1, 2, expectedHeader, expectedBody);
    }
    @Test
    public void test_display_empty_2by2() {
        String expectedHeader= "  0 | 1\n";
        String expectedBody =
                "A   |   A\n" +
                "B   |   B\n";
        emptyBoardHelper(2, 2, expectedHeader, expectedBody);
    }
    @Test
    public void test_display_empty_3by2() {
        String expectedHeader= "  0 | 1 | 2\n";
        String expectedBody =
                        "A   |   |   A\n"+
                        "B   |   |   B\n";
        emptyBoardHelper(3, 2, expectedHeader, expectedBody);
    }
    @Test
    public void test_display_empty_3by5() {
        String expectedHeader= "  0 | 1 | 2\n";
        String expectedBody =
                        "A   |   |   A\n"+
                        "B   |   |   B\n"+
                        "C   |   |   C\n"+
                        "D   |   |   D\n"+
                        "E   |   |   E\n";
        emptyBoardHelper(3, 5, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_enemy_3by5() {
        String expectedHeader= "  0 | 1 | 2\n";
        String expectedBody =
                "A   |   |   A\n"+
                        "B   |   |   B\n"+
                        "C   |   |   C\n"+
                        "D   |   |   D\n"+
                        "E   |   |   E\n";
        emptyEnemyBoardHelper(3, 5, expectedHeader, expectedBody);
    }

    @Test
    public void test_adding_ships_1by1() {
        String expectedHeader= "  0\n";
        String expectedBody =
                "A s A\n";
        Board<Character> b1 = new BattleShipBoard(1, 1,'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0), 's', '*');
        b1.tryAddShip(s1);
        notEmptyBoardHelper(expectedHeader, expectedBody, b1);
    }

    @Test
    public void test_adding_ships_3by5() {
        String expectedHeader= "  0 | 1 | 2\n";
        String expectedBody =
                        "A s |   |   A\n"+
                        "B   |   |   B\n"+
                        "C   |   |   C\n"+
                        "D   |   |   D\n"+
                        "E   |   |   E\n";
        Board<Character> b1 = new BattleShipBoard(3, 5,'X');
        RectangleShip<Character> s1 = new RectangleShip<Character>(new Coordinate(0,0), 's', '*');
        b1.tryAddShip(s1);
        notEmptyBoardHelper(expectedHeader, expectedBody, b1);

        expectedBody =
                    "A s |   |   A\n"+
                    "B   | s |   B\n"+
                    "C   |   |   C\n"+
                    "D s |   |   D\n"+
                    "E   |   | s E\n";
        RectangleShip<Character> s2 = new RectangleShip<Character>(new Coordinate(1,1), 's', '*');
        b1.tryAddShip(s2);
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(3,0), 's', '*'));
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(4,2), 's', '*'));
        notEmptyBoardHelper(expectedHeader, expectedBody, b1);
    }
    @Test
    public void test_adding_then_attacking_ships_3by5() {
        String expectedHeader= "  0 | 1 | 2\n";
        String expectedBody =
                        "A s |   |   A\n"+
                        "B s |   | X B\n"+
                        "C   |   |   C\n"+
                        "D   |   |   D\n"+
                        "E   |   |   E\n";
        Board<Character> b1 = new BattleShipBoard(3, 5,'X');
        Coordinate c0 = new Coordinate(0,0);
        Coordinate c1 = new Coordinate(1,0);
        Coordinate c2 = new Coordinate(1,1);
        RectangleShip<Character> s0 = new RectangleShip<Character>(c0, 's', '*');
        RectangleShip<Character> s1 = new RectangleShip<Character>(c1, 's', '*');
        RectangleShip<Character> s2 = new RectangleShip<Character>(c2, 's', '*');
        b1.tryAddShip(s0);
        b1.tryAddShip(s1);
        b1.tryAddShip(s2);
        b1.fireAt(c0);
        b1.fireAt(c1);
        b1.fireAt(new Coordinate(1,2));
        notEmptyEnemyBoardHelper(expectedHeader, expectedBody, b1);
    }

    @Test
    public void test_next_view_display() {
        Board<Character> b1 = new BattleShipBoard<Character>(9, 20,'X');
        Board<Character> b2 = new BattleShipBoard<Character>(9, 20,'X');
        BoardTextView view = new BoardTextView(b1);
        String s = view.displayMyBoardWithEnemyNextToIt(new BoardTextView(b2), "Your ocean", "Player B's ocean");
        System.out.print(s);
    }
}

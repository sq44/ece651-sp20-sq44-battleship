package edu.duke.sq44.battleship;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
    @Test
    public void test_constructor() {
        SimpleShipDisplayInfo s = new SimpleShipDisplayInfo(5,false);
        assertEquals(false, s.getInfo(new Coordinate(0,0), true));
        assertEquals(5, s.getInfo(new Coordinate(0,0) ,false));
    }
}

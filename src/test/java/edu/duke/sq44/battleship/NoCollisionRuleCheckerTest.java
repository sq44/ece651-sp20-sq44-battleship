package edu.duke.sq44.battleship;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class NoCollisionRuleCheckerTest {
    @Test
    public void test_all_place_rule() {
        NoCollisionRuleChecker<Character> cRule = new NoCollisionRuleChecker<>(null);
        InBoundsRuleChecker<Character> bRule = new InBoundsRuleChecker<>(cRule);
        BattleShipBoard<Character> board = new BattleShipBoard<>(4,4, 'X',bRule);
        V1ShipFactory v = new V1ShipFactory();
        Ship<Character> des1 = v.makeDestroyer(new Placement("a0v"));// make ship
        board.tryAddShip(des1); // add ship

        assertEquals("That placement is invalid: the ship overlaps another ship.\n",
                cRule.checkMyRule(des1,board));
        assertEquals("That placement is invalid: the ship overlaps another ship.\n",
                bRule.checkPlacement(des1, board));
        Ship<Character> des2 = v.makeDestroyer(new Placement("b0v"));
        assertEquals("That placement is invalid: the ship overlaps another ship.\n",
                bRule.checkPlacement(des2, board));

        Ship<Character> des5 = v.makeDestroyer(new Placement("d0v")); //not place
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.\n",
                bRule.checkPlacement(des5, board));

        Ship<Character> des6 = v.makeDestroyer(new Placement("d0h"));
        assertEquals(null, bRule.checkPlacement(des6, board));
    }
}

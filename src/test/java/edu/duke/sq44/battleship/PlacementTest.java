package edu.duke.sq44.battleship;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
    @Test
    public void test_placement_valid(){
        Coordinate c1 = new Coordinate("A5");
        Coordinate c2 = new Coordinate("A5");
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c2, 'h');
        assertThrows(IllegalArgumentException.class, () -> new Placement(c1, 'z'));

        assertEquals(p1, p2);
        assertEquals(new Placement("A5V"), new Placement("A5v"));
        assertNotEquals(new Placement("A5V"), c1);
        assertNotEquals(new Placement("A5V"), new Placement("A6v"));
        assertNotEquals(new Placement("A5V"), new Placement("A5h"));

        p3.hashCode();
        Placement p4 = new Placement("a5h");
        assertThrows(IllegalArgumentException.class, ()->new Placement("A56"));
        assertThrows(IllegalArgumentException.class, ()->new Placement("A10v"));
        assertThrows(IllegalArgumentException.class, ()->new Placement("AIv"));
    }

    @Test
    public void test_placement_valid_for_new_dir() {
        Coordinate c1 = new Coordinate("A5");
        Placement p1 = new Placement(c1, 'u');
        Placement p2 = new Placement(c1, 'd');
        Placement p3 = new Placement(c1, 'l');
        Placement p4 = new Placement(c1, 'r');
        Placement p5 = new Placement(c1, 'U');
        Placement p6 = new Placement(c1, 'D');
        Placement p7 = new Placement(c1, 'L');
        Placement p8 = new Placement(c1, 'R');
        assertEquals(new Placement("A5r"), new Placement("A5R"));
        assertThrows(IllegalArgumentException.class, ()->new Placement("A2t"));
    }
}

package edu.duke.sq44.battleship;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GeneratorTest {
    @Test
    public void testChoice() throws InterruptedException {
        IntelligentOpGenerator g = new IntelligentOpGenerator();
        while(true) {
           System.out.println(g.generateChoice());
            Thread.sleep(100);
        }
    }

    @Test
    public void testFire() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        InputStream input = getClass().getClassLoader().getResourceAsStream("inputWithSM.txt");
        assertNotNull(input);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try {
            System.setIn(input);
            System.setOut(out);
            App.eveMode();
        }
        finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }
    }
}

package edu.duke.sq44.battleship;

import edu.duke.sq44.battleship.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextPlayerTest {
    @Test
    void test_read_placement() throws IOException {
        StringReader sr = new StringReader("B2V\nC8H\na4v\n");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bytes, true);
        Board<Character> b = new BattleShipBoard<Character>(10, 20,'X');
        TextPlayer player = new TextPlayer("A", b, new BufferedReader(sr), ps, new V1ShipFactory());

        String prompt = "Player " + "A" + " where do you want to place a Destroyer?";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back?
            //assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline, TODO: error lines?
            bytes.reset(); //clear out bytes for next time around
        }
        TextPlayer playerNull = new TextPlayer("A", b, new BufferedReader(new StringReader("")), ps, new V1ShipFactory());
        //assertThrows(IllegalArgumentException.class, () -> playerNull.readPlacement(prompt));
    }

    @Test
    void test_one_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
        AbstractShipFactory<Character> shipFactory = new V2ShipFactory();
        Set<Integer> mySet = new HashSet<>();
        mySet.add(-1);
        player.doOnePlacement("Submarine", player.shipCreationFns.get("Submarine"), mySet);//place new pos
    }

    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

    @Test
    public void test_read_fire_position_eof() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer t = createTextPlayer(10,10,"cool\nC8\n", bytes);
        t.readPosition("test test");
    }

    @Test
    public void test_read_fire_action() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer t = createTextPlayer(10,10,"C8\n", bytes);
        Board<Character> enemyBoard = new BattleShipBoard<Character>(9, 15,'X');
        t.fireAction(enemyBoard);
    }

    @Test
    public void play_one_turn() throws IOException {
        ByteArrayOutputStream bytes0 = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "C8H\nF\nC8\nF\nC8\nM\nC8\na4h\n", bytes0);
        AbstractShipFactory<Character> shipFactory = new V2ShipFactory();
        Set<Integer> mySet = new HashSet<>();
        mySet.add(-1);
        player.doOnePlacement("Submarine", player.shipCreationFns.get("Submarine"), mySet);//place new pos
        Board<Character> enemyBoard = new BattleShipBoard<Character>(10, 20,'X');
        player.playOneTurn(enemyBoard, "B");
        player.playOneTurn(enemyBoard, "B");
        player.playOneTurn(enemyBoard, "B");

        ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
        TextPlayer tInvalid = createTextPlayer(10,10,"q\nS\nC8\nS\nC8\nS\nF8\nS\nC8\n" +
                "S\nC8\nS\nC8\nS\nF8\nS\nC8\nS\nC8\nS\nC8\nS\nF8\nS\nC8\n", bytes1);
    }
}

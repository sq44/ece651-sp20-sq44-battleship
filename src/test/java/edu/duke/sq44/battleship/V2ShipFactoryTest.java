package edu.duke.sq44.battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class V2ShipFactoryTest {
    @Test
    public void test_placement_battleship() {
        V2ShipFactory v = new V2ShipFactory();
        Placement pu = new Placement("a0u");
        Placement pd = new Placement("a0d");
        Placement pl = new Placement("a0l");
        Placement pr = new Placement("b0r");
        Ship<Character> batu = v.makeBattleship(pu);
        Ship<Character> batd = v.makeBattleship(pd);
        Ship<Character> batl = v.makeBattleship(pl);
        Ship<Character> batr = v.makeBattleship(pr);
        checkShip(batu, "Battleship", 'b', new Coordinate(0,1), new Coordinate(1,0),
        new Coordinate(1,1), new Coordinate(1,2));
        checkShip(batd, "Battleship",'b', new Coordinate(0,0), new Coordinate(0,1),
                new Coordinate(0,2), new Coordinate(1,1));
        checkShip(batl, "Battleship",'b', new Coordinate(0,1), new Coordinate(1,0),
                new Coordinate(1,1), new Coordinate(2,1));
        checkShip(batr, "Battleship",'b', new Coordinate(1,0), new Coordinate(2,0),
                new Coordinate(2,1), new Coordinate(3,0));


        Ship<Character> caru = v.makeCarrier(pu);
        checkShip(caru, "Carrier", 'c', new Coordinate(0,0), new Coordinate(1,0), new Coordinate(2,0),
                new Coordinate(3,0), new Coordinate(2,1), new Coordinate(3,1), new Coordinate(4,1));
        Ship<Character> card = v.makeCarrier(pd);
        checkShip(card, "Carrier", 'c', new Coordinate(0,0), new Coordinate(1,0), new Coordinate(2,0),
                new Coordinate(1,1), new Coordinate(2,1), new Coordinate(3,1), new Coordinate(4,1));
        Ship<Character> carl = v.makeCarrier(pl);
        checkShip(carl, "Carrier", 'c', new Coordinate(0,2), new Coordinate(0,3), new Coordinate(0,4),
                new Coordinate(1,0), new Coordinate(1,1), new Coordinate(1,2), new Coordinate(1,3));
        Ship<Character> carr = v.makeCarrier(pr);
        checkShip(carr, "Carrier", 'c', new Coordinate(1,1), new Coordinate(1,2), new Coordinate(1,3),
                new Coordinate(1,4), new Coordinate(2,0), new Coordinate(2,1), new Coordinate(2,2));

        assertThrows(IllegalArgumentException.class, ()-> v.makeCarrier(new Placement("a0v")));
        assertThrows(IllegalArgumentException.class, ()-> v.makeBattleship(new Placement("a0v")));
        assertThrows(IllegalArgumentException.class, ()-> v.makeDestroyer(new Placement("a0u")));
        assertThrows(IllegalArgumentException.class, ()-> v.makeDestroyer(new Placement("a0d")));
        assertThrows(IllegalArgumentException.class, ()-> v.makeSubmarine(new Placement("a0u")));
        assertThrows(IllegalArgumentException.class, ()-> v.makeSubmarine(new Placement("a0d")));

        v.makeDestroyer(new Placement("a0V"));
        v.makeSubmarine(new Placement("a0H"));
    }

    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c: expectedLocs) {
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c,true));
        }
    }

}
